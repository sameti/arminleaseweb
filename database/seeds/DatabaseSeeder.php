<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(add_default_admin_user::class);
        // $this->call(UsersTableSeeder::class);
    }
}
