<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
});

Route::group([

    'middleware' => ['api','jwt.auth'],
    'prefix' => 'servers'

], function () {
    Route::get('/{id}', 'ServerController@show');
    Route::post('/', 'ServerController@store');
    Route::get('/', 'ServerController@index');
    Route::delete('/{id}', 'ServerController@delete');
});