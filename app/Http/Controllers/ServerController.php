<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiErrorException;
use App\Models\Ram;
use App\Models\Server;
use App\Rules\RamRule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ServerController extends Controller
{
    /**
     * @return Server[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Server::all();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($assetId)
    {
        try {
            $server = Server::where('asset_id', $assetId)->with('rams')->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            throw new ApiErrorException(
                Response::HTTP_NOT_FOUND,
                'Record Not Found',
                Response::HTTP_NOT_FOUND
            );
        }
        return response()->json($server->toArray(), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'asset_id' => 'required|unique:servers',
            'price' => 'required|numeric|min:1',
            'rams' => ['required',new RamRule]
        ]);
        if($validator->fails()) {
            throw new ApiErrorException(Response::HTTP_UNPROCESSABLE_ENTITY,$validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $server = Server::create([
            'asset_id' => $request->get('asset_id'),
            'price' => $request->get('price'),
            'name' => $request->get('name'),
            'brand' => $request->get('brand'),
        ]);
        foreach ($request->get('rams') as $ram) {
            Ram::create([
                'server_id' => $server->id,
                'type' => $ram['type'],
                'size' => $ram['size'],
                'count' => $ram['count'] ?? 1
            ]);
        }

        return response()->json(
            Server::where('id',$server->id)->with('rams')->first()->toArray(),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return int
     */
    public function delete(Request $request, $assetId)
    {
        $article = Server::where('asset_id', $assetId)->firstOrFail();
        $article->delete();

        return response()->json([],Response::HTTP_NO_CONTENT);
    }
}
