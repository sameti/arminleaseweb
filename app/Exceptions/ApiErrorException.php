<?php

namespace App\Exceptions;

use Exception;

class ApiErrorException extends Exception
{

    /**
     * @var
     */
    private $errorCode;
    /**
     * @var
     */
    private $errorMessage;
    /**
     * @var
     */
    private $httpStatusCode;


    /**
     * ApiErrorException constructor.
     * @param int $errorCode
     * @param string $errorMessage
     * @param int $httpStatusCode
     */
    public function __construct($errorCode, $errorMessage, $httpStatusCode = 400)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->httpStatusCode = $httpStatusCode;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param mixed $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return mixed
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * @param mixed $httpStatusCode
     */
    public function setHttpStatusCode($httpStatusCode)
    {
        $this->httpStatusCode = $httpStatusCode;
    }
}
