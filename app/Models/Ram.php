<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ram extends Model
{
    protected $fillable = ['server_id','type','size','count','created_at'];

    protected $hidden = ['id', 'server_id'];
}
