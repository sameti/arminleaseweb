<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = ['asset_id','price','brand','created_at'];

    protected $hidden = ['id'];

    public function rams()
    {
        return $this->hasMany(Ram::class,'server_id','id');
    }
}
