<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RamRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /*
         * Logic for checking the Ram input structure
         */
        $checkValidRam = 0;
        foreach ($value as $ram) {
            //check the both fields exists
            if(!isset($ram["type"]) || !isset($ram["size"])) {
                return 0;
            }
            //check type
            if($ram["type"] != 'DDR4' && $ram["type"] != 'DDR3') {
                return 0;
            }
            //check type be a correct number
            if(!$this->isPowerOfTwo($ram["size"])) {
                return 0;
            }
            $checkValidRam = 1;
        }
        return $checkValidRam;
    }

    private function Log2($x)
    {
        return (log10($x) /
            log10(2));
    }
    private function isPowerOfTwo($n)
    {
        return (ceil($this->Log2($n)) ==
            floor($this->Log2($n)));
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must have both correct size and type';
    }
}
